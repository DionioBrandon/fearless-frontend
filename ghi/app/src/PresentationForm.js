import React, { useEffect, useState } from 'react';

function PresentationForm(props) {
    const [conferences, setConferences] = useState([]);
    const [presentername, setPresenterName] = useState('');
    const [presenteremail, setPresenterEmail] = useState('');
    const [companyname, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('')

    const handleConference = (event) => {
        const value = event.target.value;
        setConference(value);
    }

    const handlePresenterName = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const handlePresenterEmail = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();

        const data = {};
        data.presenter_name = presentername;
        data.presenter_email = presenteremail;
        data.company_name = companyname;
        data.title = title;
        data.synopsis = synopsis;
        console.log(data);

        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {

            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input value={presentername} onChange={handlePresenterName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={presenteremail} onChange={handlePresenterEmail} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={companyname} onChange={handleCompanyName} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={title} onChange={handleTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea value={synopsis} onChange={handleSynopsis} id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConference} required name="conference" id="conference" className="form-select">
                            <option value={conference} >Choose a conference</option>
                                {conferences.map(conference => {
                                    return (
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>

    );
}

export default PresentationForm;
